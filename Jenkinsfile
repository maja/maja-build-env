pipeline {
  agent {
    node { label 'docker' }
  }
  options {
    gitLabConnection('gitlab.cnes.fr')
    gitlabBuilds(builds: ['docker-build'])
  }
  environment {
    dockerRegistry = "registry.gitlab.cnes.fr/maja/maja-build-env"
    dr_opts = "--build-arg http_proxy=http://surf.loc.cnes.fr:8050 --build-arg https_proxy=http://surf.loc.cnes.fr:8050"
  }
  stages{
    stage("docker-build") {
      steps{
        // deploy CNES certificates for proxy
        sh 'mkdir -p Ubuntu_16.04/certs'
        sh 'mkdir -p CentOS_6/certs'
        sh 'mkdir -p CentOS_7/certs'
        sh 'cp /etc/pki/ca-trust/source/anchors/AC*.crt Ubuntu_16.04/certs/'
        sh 'cp /etc/pki/ca-trust/source/anchors/AC*.crt CentOS_6/certs/'
        sh 'cp /etc/pki/ca-trust/source/anchors/AC*.crt CentOS_7/certs/'
        // build images
        sh 'docker build ${dr_opts} -t maja-env-ubuntu:16.04 Ubuntu_16.04'
        sh 'docker build ${dr_opts} -t maja-env-centos:6 CentOS_6'
        sh 'docker build ${dr_opts} -t maja-env-centos:7 CentOS_7'
      }
      post {
        failure { updateGitlabCommitStatus name: 'docker-build', state: 'failed'}
        success { updateGitlabCommitStatus name: 'docker-build', state: 'success'}
      }
    }
    stage("docker-push") {
      when {
        expression { return env.gitlabSourceBranch == "master"}
        expression { return env.gitlabSourceNamespace == "maja"}
        }
      steps{
        sh "docker tag maja-env-ubuntu:16.04 ${dockerRegistry}/maja-env-ubuntu:16.04"
        sh "docker push ${dockerRegistry}/maja-env-ubuntu:16.04"
        sh "docker tag maja-env-centos:6 ${dockerRegistry}/maja-env-centos:6"
        sh "docker push ${dockerRegistry}/maja-env-centos:6"
        sh "docker tag maja-env-centos:7 ${dockerRegistry}/maja-env-centos:7"
        sh "docker push ${dockerRegistry}/maja-env-centos:7"
      }
      post {
        failure { updateGitlabCommitStatus name: 'docker-push', state: 'failed'}
        success { updateGitlabCommitStatus name: 'docker-push', state: 'success'}
      }
    }
  }
}
