# Dockerized build environment for Maja

This repository contains Dockerfiles for Maja build environments:

* RedHat 8 (Rockylinux)
* Ubuntu 20.04 Focal
* Ubuntu 22.04 Jammy

They will be used for Maja CI.
